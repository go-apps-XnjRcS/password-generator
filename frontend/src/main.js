import 'babel-polyfill';
import Vue from 'vue';
import VueClipboard from 'vue-clipboard2'


// Setup Vuetify
import Vuetify from 'vuetify';
Vue.use(Vuetify);

VueClipboard.config.autoSetContainer = true;
Vue.use(VueClipboard);


import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont';

import App from './App.vue';

Vue.config.productionTip = false;
Vue.config.devtools = true;

import * as Wails from '@wailsapp/runtime';

Wails.Init(() => {
	new Vue({
		render: h => h(App)
	}).$mount('#app');
});
