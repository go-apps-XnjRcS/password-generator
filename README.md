## Password generator
Simple (GUI) tool for generation password. 

![alt password generator demo](./media/screen.png)

### Opportunities
* set password length from 8-64 characters 
* include / exclude digits and symbols

### Download binary
* linux [passGen download](https://upfiler.sht-server.cf/#/download/?t=file&h=199592c963ed812)
* windows [passGen.exe download](https://upfiler.sht-server.cf/#/download/?t=file&h=6b9271c3fefc3dc)

### How to use (linux)
* download the app binary
* set it executable `chmod +x passGen`
* move to bin folder `sudo mv passGen /usr/local/bin`
* run and enjoy `passGen`

### How to build from source
* [install Wails](https://wails.app/gettingstarted/) framework
* download or clone the source
* run `wails build` wails will build binary for you 

### Thanks (dependencies)
* [Wails app](https://wails.app) 
* [go-password](https://github.com/sethvargo/go-password)
