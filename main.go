package main

import (
	"github.com/leaanthony/mewn"
	"github.com/sethvargo/go-password/password"
	"github.com/wailsapp/wails"
	"log"
)

func basic(length int, digits bool, sybmols bool) (string, error) {

	digitsNum := 0
	sybmolsNum := 0

	if digits {
		digitsNum = length / 6
	}

	if sybmols {
		sybmolsNum = length / 6
	}

	res, err := password.Generate(length, digitsNum, sybmolsNum, false, true)

	if err != nil {
		return "", err
	}

	return res, nil
}

func main() {

	log.SetFlags(log.Lshortfile)

	js := mewn.String("./frontend/dist/app.js")
	css := mewn.String("./frontend/dist/app.css")

	app := wails.CreateApp(&wails.AppConfig{
		Width:     1024,
		Height:    768,
		Title:     "Password generator",
		JS:        js,
		CSS:       css,
		Colour:    "#131313",
		Resizable: true,
	})
	app.Bind(basic)
	app.Run()
}
