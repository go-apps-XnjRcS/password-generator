module passGen

require (
	github.com/leaanthony/mewn v0.10.7
	github.com/sethvargo/go-password v0.1.3
	github.com/wailsapp/wails v1.0.1
)

go 1.13
