package main

import (
	"log"
	"testing"
)

func Test_basic(t *testing.T) {

	log.SetFlags(log.Lshortfile)

	got, err := basic(10, true, true)
	if err != nil {
		t.Error(err)
	} else {
		log.Printf("Generated: %q", got)
	}
}
